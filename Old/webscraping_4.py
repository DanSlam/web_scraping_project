#import matplotlib
import pymysql
from pymysql import connect
import pandas as pd
import datetime
from sqlalchemy import create_engine

# fill in your username
#username = 'root@localhost'


def import_to_sql_database(list_of_dictionnary):
    # Connect to the database
    cnx = pymysql.connect(host='localhost',
                                 user='root',
                                 password='ITC2018O')
    # Create a cursor object
    cursorObject = cnx.cursor()
    #Drop our Database if it's already exist and create Zabilo Database
    #query_drop_database = "DROP DATABASE IF EXISTS ZABILO;"
    query_database = "CREATE DATABASE IF NOT EXISTS ZABILO;"

    # Execute the sqlQuery
    cursorObject.execute(query_drop_database)
    cursorObject.execute(query_database)

    # Use the database
    use = "USE ZABILO"

        # run the query_use
    cursorObject.execute(use)

    # Create our Table Zabilo_Price in under to follow the evolution of price
    query_drop_table = "DROP TABLE IF EXISTS zabilo_price;"
    query_table_products = "CREATE TABLE zabilo_price (Reference CHAR(100) PRIMARY KEY NOT NULL,Product CHAR(100),Price INT,OldPrice INT, Date CHAR(100));"

    # Execute the sqlQuery
    cursorObject.execute(query_drop_table)
    cursorObject.execute(query_table_products)

    # SQL query to insert values in the columns of Zabilo_Price table
    query ='INSERT INTO zabilo_price (Reference, Product,Price,OldPrice) VALUES (%(Reference)s, %(Product)s, %(Price)s,%(Old Price)s) ON DUPLICATE KEY UPDATE Price = (Price) ;'
    cursorObject.executemany(query,list_of_dictionnary)
    cnx.commit()

    query_update =" UPDATE Zabilo_Price SET Reference = %(Reference)s, Price =%(Price)s, OldPrice = %(Old Price)s WHERE Reference = %(Reference)s ;"
    cursorObject.executemany(query_update,list_of_dictionnary)
    cnx.commit()


query_drop_table = "DROP TABLE IF EXISTS zabilo_price;"
query_table_products = "CREATE TABLE zabilo_price (Reference CHAR(100) PRIMARY KEY NOT NULL,Product CHAR(100),Price INT,OldPrice INT, Date DATETIME(6));"

     # Execute the sqlQuery
     cursorObject.execute(query_drop_table)
     cursorObject.execute(query_table_products)
list_of_dictionnary = [{'Product': 'Washing machine',
  'Reference': 'CWT10R16IL',
  'Price': 1699.0,
  'Old Price': 1999.0,
  'Date': '2018-11-07 19:59'},
 {'Product': 'Washing machine',
  'Reference': 'HW80-1203',
  'Price': 1099.0,
  'Old Price': 1590.0,
  'Date': '2018-11-07 19:59'},
 {'Product': 'Air conditioner',
  'Reference': 'Platinum 140+',
  'Price': 1849.0,
  'Old Price': 2190.0,
  'Date': '2018-11-07 19:59'},
 {'Product': 'TV',
  'Reference': 'FJ-554K',
  'Price': 1799.0,
  'Old Price': 2499.0,
  'Date': '2018-11-07 19:59'}]
     # SQL query to insert values in the columns of Zabilo_Price table
     query ='INSERT IGNORE INTO zabilo_price (Reference, Product,Price,OldPrice, Date) VALUES (%(Reference)s, %(Product)s, %(Price)s,%(Old Price)s,%(Date)s);'
     cursorObject.executemany(query,list_of_dictionnary)
     cnx.commit()



list_of_dictionnary =[{'Product': 'Washing machine',
  'Reference': 'CWT10R16IL',
  'Price': 1699.0,
  'Old Price': 1999.0,
  'Date': '2018-11-07 19:59'},
 {'Product': 'Washing machine',
  'Reference': 'HW80-1203',
  'Price': 1099.0,
  'Old Price': 1590.0,
  'Date': '2018-11-07 19:59'},
 {'Product': 'Air conditioner',
  'Reference': 'Platinum 140+',
  'Price': 1849.0,
  'Old Price': 2190.0,
  'Date': '2018-11-07 19:59'},
 {'Product': 'TV',
  'Reference': 'FJ-554K',
  'Price': 17000.0,
  'Old Price': 2499.0,
  'Date': '2018-11-07 21:00'}]


         query ='INSERT INTO zabilo_price (Reference, Product,Price,OldPrice, Date) VALUES (%(Reference)s, %(Product)s, %(Price)s,%(Old Price)s,%(Date)s) ON DUPLICATE KEY UPDATE Price = (Price) ;'
         cursorObject.executemany(query,list_of_dictionnary)
         cnx.commit()

         query_update =" UPDATE Zabilo_Price SET Reference = %(Reference)s, Price =%(Price)s, OldPrice = %(Old Price)s, Date = %(Date)s WHERE Reference = %(Reference)s AND Price <> %(Price)s ;"
         cursorObject.executemany(query_update,list_of_dictionnary)
         cnx.commit()
