# Connect to the database
import requests
from bs4 import BeautifulSoup
import pandas as pd
import click
import pymysql
from pymysql import connect
import datetime


cnx = pymysql.connect(host='localhost',
                             user='root',
                             password='ITC2018O')
# Create a cursor object
cursorObject = cnx.cursor()

# Use the database
use = "USE ZABILO"

    # run the query_use
cursorObject.execute(use)

# Create our Table Zabilo_Price in under to follow the evolution of price and stock
query_drop_table = "DROP TABLE IF EXISTS zabilo_reviews;"
query_table_products = "CREATE TABLE IF NOT EXISTS zabilo_reviews (Reference CHAR(100) PRIMARY KEY NOT NULL,Product CHAR(100),Ratings CHAR(100) ,Reviews VARCHAR(100), Date DATETIME(6));"

# Execute the sqlQuery
cursorObject.execute(query_drop_table)
cursorObject.execute(query_table_products)

# SQL query to insert values in the columns of Zabilo_Price table
query_2 = "SET Names UTF8;"
cursorObject.execute(query_2)
query ="INSERT INTO zabilo_reviews (Reference, Product, Ratings, Reviews, Date) VALUES (%(Reference)s, %(Product)s, %(Ratings)s, %(Reviews)s, %(Date)s) ON DUPLICATE KEY UPDATE Reference = (Reference) ;"
cursorObject.executemany(query,list_of_dictionnary)
cnx.commit()

 # Update our Database if the price is changing from the last scrapping
query_update ="UPDATE zabilo_reviews SET Reference = %(Reference)s, Ratings = %(Ratings)s, Reviews = %(Reviews)s, Date = %(Date)s WHERE Reference = %(Reference)s AND (Reviews <> %(Reviews)s OR (Ratings <> %(Ratings)s));"
cursorObject.executemany(query_update,list_of_dictionnary)
cnx.commit()


query ="INSERT INTO zabilo_reviews (Reference, Product, Ratings, Reviews, Date) VALUES (%(Reference)s, %(Product)s, %(Ratings)s, %(Reviews)s, %(Date)s) WHERE Reference = %(Reference)s AND (Reviews <> %(Reviews)s OR Ratings <> %(Ratings)s)"
cursorObject.executemany(query,list_of_dictionnary)
cnx.commit()



list_of_dictionnary =[{'Product': 'Washing machine', 'Ratings': 'Nneorns', 'Reference': 'CWTzaj10R16IL', 'Reviews': '[]', 'Date': '2018-11-10 19:34'}, {'Product': 'Washing machine', 'Ratings': 24, 'Reference': 'HW80-1203', 'Reviews': "['ממליצה בחום ', 'מרוצה', 'חוות דעת ', 'Super']", 'Date': '2018-11-10 19:34'}, {'Product': 'Washing machine', 'Ratings': 'No ratings', 'Reference': 'Platinum +140', 'Reviews': '[]', 'Date': '2018-11-10 19:34'}, {'Product': 'Washing machine', 'Ratings': 'No ratings', 'Reference': 'FJ554K', 'Reviews': '[]', 'Date': '2018-11-10 19:34'}]
