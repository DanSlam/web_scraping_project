### Import of packages
import requests
import numpy as np
from bs4 import BeautifulSoup
import pandas as pd

url_home_page = "https://www.zabilo.com/en/"

def start(url_home_page):
    # This funtion allow to scrap the url of "Today's Hot Deal"
    url_home_page = requests.get(url_home_page)
    content_of_home = url_home_page.content
    soup_of_home = BeautifulSoup(content_of_home, 'html.parser')
    link_of_deals = [i['href'] for i in soup_of_home.select('a[class*="navdeals"]')]
    return link_of_deals


def deal_pages(url_deal):
    # This function allow to scrapp the url of the different category of product
    url_deal = requests.get(url_deal[0])
    content_deal = url_deal.content
    soup_of_deal = BeautifulSoup(content_deal, 'html.parser')
    link_of_category_of_product =[]
    for category in soup_of_deal.select('[class="landing_categories_little"]'):
        for http in category.select('a'):
            link_of_category_of_product.append(http['href'])
    return(link_of_category_of_product)


def link_of_product(list_of_url):
    # We scrapp the link of product sheet
    list_of_product = []
    for url in list_of_url:
        url_product_machine = requests.get(url)
        content_product = url_product_machine.content
        soup_of_product = BeautifulSoup(content_product, 'html.parser')
        for category in soup_of_product.select('a[class*="product-name"]'):
            list_of_product.append(category['href'])
    return list_of_product


def get_product_characteristics(list_url):
    # This function allows to get the characteristics for each url of product
    list_product=[]
    for url in list_url:

        r = requests.get(url)
        c = r.content
        soup = BeautifulSoup(c, 'html.parser')

        table = soup.find_all("table", {"class": "table-data-sheet data_sheet"})[0]

        char_dict = {}
        characteristics = []

        for row in table.find_all('tr'):
            element = row.find_all('td')
            for td in element:
                characteristics.append(td.text)

            keys = []
            values = []
            for i in range(0, len(characteristics)):
                if i % 2 == 0:
                    keys.append(characteristics[i])
                else:
                    values.append(characteristics[i])

            keys = keys[0:14]
            values = values[0:14]

            ## add price
            keys.append('Price')
            values.append(soup.find('span', attrs = {'itemprop':'price'}).get_text())
            keys.append('Old Price')
            values.append(soup.find('span', attrs = {'id':'old_price_display'}).get_text())

            char_dict = {k: v for k, v in zip(keys, values)}
        list_product.append(char_dict)
    return list_product


def get_data_frame(list_product):
    # We creat thanks to this function a dataframe with every characteristics for each product
    df = pd.DataFrame(list_product)
    df.groupby("Product")
    return df

urls = ["https://www.zabilo.com/en/top-loading-washing-machines/2629-constructa-top-loading-washer-6kg-1000rpm-cwt10r16il.html","https://www.zabilo.com/en/front-loading-washing-machines/2423-washing-machine-haier-hw80-1203-8kg-1200-rpm.html","https://www.zabilo.com/en/top-air-conditionner/3196-electra-air-conditioner-125-hp-12300-btu-platinum-140.html","https://www.zabilo.com/en/tvs-55/2308-fujicom-smart-tv-55-inches-4k-ultra-hd-fj554k.html"]



def splitframe(data, name='Product'):

    n = data[name][0]

    df = pd.DataFrame(columns=data.columns)

    datalist = []

    for i in range(len(data)):
        if data[name][i] == n:
            df = df.append(data.iloc[i])
        else:
            datalist.append(df)
            df = pd.DataFrame(columns=data.columns)
            n = data[name][i]
            df = df.append(data.iloc[i])

    return datalist

def export_csv(data):
    for i in data:
        i.to_csv(r"C:\Users\dansl\Desktop\ITC_October_18\Data_mining\data2.csv", sep = ";")

export_csv(splitframe(get_data_frame(get_product_characteristics(urls))))
