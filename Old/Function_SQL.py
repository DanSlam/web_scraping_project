
def import_to_sql_database(list_of_dictionnary):
    # Connect to the database
    cnx = pymysql.connect(host='localhost',
                                 user='root',
                                 password='ITC2018O')
    # Create a cursor object
    cursorObject = cnx.cursor()
    #Drop our Database if it's already exist and create Zabilo Database
    query_database = "CREATE DATABASE IF NOT EXISTS ZABILO;"

    # Execute the sqlQuery
    #cursorObject.execute(query_drop_database)
    cursorObject.execute(query_database)

    # Use the database
    use = "USE ZABILO"

        # run the query_use
    cursorObject.execute(use)

    # Create our Table Zabilo_Price in under to follow the evolution of price
    #query_drop_table = "DROP TABLE IF EXISTS zabilo_price;"
    query_table_products = "CREATE TABLE IF NOT EXISTS ZABILO_PRICE (Reference CHAR(100) PRIMARY KEY NOT NULL,Product CHAR(100),Price INT,OldPrice INT, Date DATETIME(6));"

    # Execute the sqlQuery
    #cursorObject.execute(query_drop_table)
    cursorObject.execute(query_table_products)

    # SQL query to insert values in the columns of Zabilo_Price table
    query ='INSERT INTO zabilo_price (Reference, Product,Price,OldPrice, Date) VALUES (%(Reference)s, %(Product)s, %(Price)s,%(Old Price)s,%(Date)s) ON DUPLICATE KEY UPDATE Price = (Price) ;'
    cursorObject.executemany(query,list_of_dictionnary)
    cnx.commit()

     # Update our Database if the price is changing from the last scrapping
    query_update =" UPDATE Zabilo_Price SET Reference = %(Reference)s, Price =%(Price)s, OldPrice = %(Old Price)s, Date = %(Date)s WHERE Reference = %(Reference)s AND (Price <> %(Price)s OR OldPrice <> %(Old Price)s) ;"
    cursorObject.executemany(query_update,list_of_dictionnary)
    cnx.commit()
