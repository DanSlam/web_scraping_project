""" This code is under copyright of Elisa Krammer and Dan Slama """

In this part of the project you will find 2 SQL tables that we have created to keep track of the price for each of our product.
If the price on the website is updated, then the SQL table is updated. If a new product appears in the website, then our database add a new entry.

The second table gathers the reviews and ratings for each product. This will help us to evaluating the popularity of each items in the next parts of the projects. 